<?php

namespace WPDesk\ABTesting\Tests\Unit\Stub;

class StubEqualGroupsRandomABTest extends \WPDesk\ABTesting\ABTest\EqualGroupsRandomABTest {
	public function get_variant() {
		return new StubBasicABVariant( $this->current_variant_id );
	}

}