<?php

namespace WPDesk\ABTesting\Tests\Unit\Stub;


use WPDesk\ABTesting\ABVariant\BasicABVariant;

class StubBasicABVariant extends BasicABVariant {
	public function is_on( $functionality ) {
		return true;
	}
}