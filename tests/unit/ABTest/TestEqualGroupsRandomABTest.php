<?php

use WPDesk\ABTesting\Tests\Unit\Stub\StubEqualGroupsRandomABTest;
use WPDesk\Persistence\Adapter\ArrayContainer;

class TestEqualGroupsRandomABTest extends PHPUnit_Framework_TestCase {

	const WHAT_DEVIATION_IS_PERMITTED = 10;
	const CONTAINER_KEY_FOR_TEST_X = 'ab_test_x_variant_id';
	const TEST_NAME_X = 'x';
	const BIG_NUMBER = 9999999;

	public function test_can_save_to_container() {
		$container = new ArrayContainer();
		new StubEqualGroupsRandomABTest( 1234, self::TEST_NAME_X, $container );
		$this->assertNotEmpty( $container->get( self::CONTAINER_KEY_FOR_TEST_X ), "Test should save variant in container" );
	}

	public function test_can_get_from_container() {
		$container = new ArrayContainer();
		$first_id = (new StubEqualGroupsRandomABTest( self::BIG_NUMBER, self::TEST_NAME_X, $container ))->get_variant()->get_variant_id();
		$second_id = (new StubEqualGroupsRandomABTest( self::BIG_NUMBER, self::TEST_NAME_X, $container ))->get_variant()->get_variant_id();

		$this->assertEquals($first_id, $second_id, "Ids for the same container should be the same");
	}

	public function test_can_do_random() {
		$sum = 0;
		for ( $i = 0; $i < 50; $i ++ ) {
			$test = new StubEqualGroupsRandomABTest( 2, 'random', new ArrayContainer() );
			$sum  += (int) $test->get_variant()->get_variant_id();
		}
		$this->assertTrue( abs( $sum - 75 ) < self::WHAT_DEVIATION_IS_PERMITTED,
			'We draw 50 numbers. Each is number 1 or 2. If chances are 50% then sum should be close to 75' );
	}
}
