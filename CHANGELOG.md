## [2.0.0] - 2020-06-05
### Changed
- upgraded wpdesk/wp-persistence library to 2.0

## [1.0.0] - 2019-02-04
### Added
- first stable version