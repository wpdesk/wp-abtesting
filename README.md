[![pipeline status](https://gitlab.com/wpdesk/wp-abtesting/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-abtesting/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-abtesting/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-abtesting/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-abtesting/v/stable)](https://packagist.org/packages/wpdesk/wp-abtesting) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-abtesting/downloads)](https://packagist.org/packages/wpdesk/wp-abtesting) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-abtesting/v/unstable)](https://packagist.org/packages/wpdesk/wp-abtesting) 
[![License](https://poser.pugx.org/wpdesk/wp-abtesting/license)](https://packagist.org/packages/wpdesk/wp-abtesting)

WordPress Library to facilitate AB Testing
===================================================


## Requirements

PHP 5.6 or later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require wpdesk/wp-abtesting
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once 'vendor/autoload.php';
```

## Compatiblity between plugins

To ensure that always the latest and valid version of composer libraries are loaded in WP env you should use a solution
that ensure support between plugins and at least decreases the change that something would break. At the moment we recommend
using wpdesk/wp-autoloader.


## Manual instalation

If you do not wish to use Composer and wpdesk/wp-autoloader, you probably should stop using any existing library as it breaks compatibility between plugins.

## Getting Started

## Project documentation

PHPDoc: https://wpdesk.gitlab.io/wp-abtesting/index.html  